/*printf ascii numbers from 1 to 500*/

#include<stdio.h>
#include<math.h>
int main()
{
  int i=1;

  int temp;
  int digit;
  int sum;

  while(i<=500)
    {
      temp=i;
      sum=0;
      
      while(temp!=0)
	{
	  
	  digit = temp%10;
	  sum = sum + pow(digit,3);
	  temp=temp/10;
	}
      
      if(sum==i)
	printf("%d is an armstrong number\n",i);
      
      /*printf("The sum is %d\n",sum);*/

      i++;
    }

  return 0;
}
