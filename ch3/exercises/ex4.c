/*print ascii values and their equivalent characters*/

#include<stdio.h>

int main()
{
  int ascii=0;

  while(ascii<=255)
    {
      printf("ASCII=%d Character = %c\n",ascii,ascii);
      ascii++;
    }
  return 0;
}
