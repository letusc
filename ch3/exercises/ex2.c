/*factorial of a given number*/


#include<stdio.h>

int main()
{
  int num;
  int i;
  long fact;
  printf("Enter a number:");
  scanf("%d",&num);

  i=1;
  fact = 1;
  while(i<=num)
    {
      fact = fact*i;
      i++;
    }
  printf("The factorial is %ld\n",fact);
  printf("Bye\n");
  return 0;
}
