/*while loop with decrementing counter*/

#include<stdio.h>

int main()
{
  int i=5;
  while(i>=1)
    {
      printf("I:%d\n",i);
      i--;
    }
  return 0;
}
