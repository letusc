#include<stdio.h>

/* calculation of total expenses*/


int main(void)
{
	int qty;
	float rate;
	int dis;
	float expense;
	
	printf("Enter quantity and rate per item: ");
	scanf("%d %f",&qty,&rate);
	
	if(qty>1000)
		dis = 10;
	
	expense = (rate*qty)-(0.10*rate*qty);
	
	printf("The total expense is %.2f\n",expense);
	
	return 0;
}
