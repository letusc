#include<stdio.h>

int main()
{

	float basic,gross,hra,da;

	printf("Enter the basic salary: ");
	scanf("%f",&basic);

	if(basic<1500)
	{
		hra=basic*10/100;
		da=basic*90/100;
	}
	else
	{
		hra=500;
		da=0.98*basic;
	}
	gross=basic+hra+da;
	printf("The gross salary is %f\n",gross);
	return 0;
}
