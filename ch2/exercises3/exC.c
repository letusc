/*greatest of three numbers*/

#include<stdio.h>

int main()
{
	int n1,n2,n3;
	int max;
	printf("Enter any three numbers:");
	scanf("%d %d %d",&n1,&n2,&n3);

	max=(n1>n2 && n1>n3 ?n1:(n2>n1 && n2>n3 ? n2 : n3));

	printf("Maximum:%d\n",max);
	return 0;
}
