#include<stdio.h>

int main()
{
	char c;

	printf("Enter a character:");
	scanf("%c",&c);

	((c>=0 && c<=47)||(c>=58 && c<=64)||(c>=91 && c<=96)||(c>=123 && c<=127)?
		printf("It is a special symbol\n"):
		printf("It is not a special symbol\n"));

	return 0;
}
