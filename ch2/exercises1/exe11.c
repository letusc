/*To find whether a point lies on,inside or outside a circle */

#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int main()
{
	int x1,y1,rad;
	int x,y;
	float d;
	int temp;

	printf("Enter the center of the circle X1, Y1 :");
	scanf("%d %d",&x1,&y1);
	printf("Enter the radius of the circle:");
	scanf("%d",&rad);

	printf("Enter the point (x,y):");
	scanf("%d %d",&x,&y);

	/*computing the distance between the two points*/
	temp = pow(x1-x,2)+pow(y1-y,2);
	d = sqrt(temp);


	if(d==rad)
		printf("The point (%d,%d) lies on the circle\n",x,y);
	else if(d<rad)
		printf("The point lies inside the circle\n");
	else 
		printf("The point lies outside the circle\n");
	
	return 0;
}
