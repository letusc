/* to check whether all the three points lie on a straight line */

#include<stdio.h>

int main()
{
	int x1,y1,x2,y2,x3,y3;
	float area;
	printf("Enter X1,Y1:");
	scanf("%d %d",&x1,&y1);

	printf("Enter X2,Y2:");
	scanf("%d %d",&x2,&y2);

	printf("Enter X3,Y3:");
	scanf("%d %d",&x3,&y3);

	/*compute the area formed by these three points. if
	it is zero , then they all lie on a same line i.e they don form a triangle */
	area = (1/2)*((x1*(y2-y3))+(x2*(y3-y1))+(x3*(y1-y2)));

	if(area == 0)
		printf("They lie on a straight line\n");
	else
		printf("They dont lie on a straight line\n");

	printf("Press any key to exit\n");
	getch();
	return 0;
}
