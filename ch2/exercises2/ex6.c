/*check for isoceles,equilateral,right triangle*/

#include<stdio.h>
#include<math.h>

int main()
{
	float s1,s2,s3;
	float temp1,temp2,temp3;
	printf("Enter the three sides of triangle: ");
	scanf("%f %f %f",&s1,&s2,&s3);

	
	/*checking equality*/
	if(s1==s2 && s2==s3)
		printf("This is an equilateral triangle.\n");
	else if(s1==s2 || s2==s3 || s3==s1)
		printf("This is an isoceles triangle.\n");
	else 
		printf("This is a scalene triangle\n");
	

	temp1 = sqrt(pow(s2,2)+pow(s3,2));
	temp2 = sqrt(pow(s1,2)+pow(s3,2));
	temp3 = sqrt(pow(s2,2)+pow(s1,2));
	if(s1==temp1 || s2==temp2 || s3==temp3)
		printf("It is an right triangle\n");

	return 0;
}
