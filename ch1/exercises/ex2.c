/*distance problem*/

#include<stdio.h>

int main()
{
  float km;
  float m,feet,inches,cm;

  printf("Enter the distance in Km: ");
  scanf("%f",&km);

  m=km*1000;
  feet = 3280.8399*km;
  inches = km * 39370.0787;
  cm = m *100;

  printf("m=%.2f\n",m);
  printf("feet=%.2f\n",feet);
  printf("inches=%.2f\n",inches);
  printf("cm=%.2f\n",cm);
  return 0;
}
