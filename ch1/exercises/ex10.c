/*population problem*/

#include<stdio.h>

int main()
{
  long pop=80000,lit_pop,ilit_pop;
  int lit_men,ilit_men,lit_women,ilit_women;
  int men,women;

  men = pop * 0.52;
  women = pop - men;

  lit_pop = 0.48 * pop;
  ilit_pop = pop - lit_pop;

  lit_men = 0.35*pop;
  lit_women = lit_pop-(0.35*pop);

  printf("Literate population: %d\n",lit_pop);
  printf("Literate men:%d\n",lit_men);
  printf("Literate women: %d\n",lit_women);

  ilit_men = men - lit_men;
  ilit_women = ilit_pop - ilit_men;

  printf("Illeterate men : %d\n",ilit_men);
  printf("Illeterate women: %d\n",ilit_women);

  return 0;
}
