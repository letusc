/*area of circle and rectangle*/

#include<stdio.h>

int main()
{
  float len,bre,rad;
  float area_rect,area_circle;
  float perimeter,cir_circle;
  printf("Enter the len and bre of rectangle: ");
  scanf("%f %f",&len,&bre);
  printf("Enter the radius of the circle: ");
  scanf("%f",&rad);

  area_rect = len*bre;
  area_circle = 3.14*rad*rad;

  perimeter = 2*(len+bre);
  cir_circle = 2*3.14*rad;

  printf("Area of Rectangle=%.2f perimeter=%.2f\n",area_rect,perimeter);
  printf("Area of circle=%.2f cir_cirle=%.2f\n",area_circle,cir_circle);

  return 0;

}
